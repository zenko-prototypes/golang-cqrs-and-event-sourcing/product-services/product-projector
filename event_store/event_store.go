package event_store

import (
	"context"
	"github.com/EventStore/EventStore-Client-Go/esdb"
)

func Connect() (*esdb.Client, error) {
	settings, err := esdb.ParseConnectionString("esdb://localhost:2113?tls=false")

	if err != nil {
		return nil, err
	}

	return esdb.NewClient(settings)
}

func Subscribe(client *esdb.Client) (*esdb.Subscription, error) {
	return client.SubscribeToAll(context.Background(), esdb.SubscribeToAllOptions{
		Filter: &esdb.SubscriptionFilter{
			Type:     esdb.StreamFilterType,
			Prefixes: []string{"product_"},
		},
	})
}

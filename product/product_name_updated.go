package product

import (
	"ProductProjectorService/interfaces"
	"fmt"
	"github.com/elastic/go-elasticsearch/v8"
	"strings"
)

type NameUpdated struct {
	interfaces.BaseEvent
	ProductName string `json:"product_name"`
}

func (e *NameUpdated) Handle(client *elasticsearch.Client) error {
	res, err := client.Update("product", e.AggregateID.String(), strings.NewReader(fmt.Sprintf(`{ "doc": { "name": "%s" }}`, e.ProductName)))
	if err != nil {
		return err
	}
	err = res.Body.Close()
	return err
}

func (e *NameUpdated) GetName() string {
	return "ProductNameUpdated"
}

package product

import (
	"ProductProjectorService/interfaces"
	"fmt"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/google/uuid"
	"strings"
)

type Created struct {
	interfaces.BaseEvent
	ProductID   uuid.UUID `json:"product_id"`
	ProductName string    `json:"product_name"`
}

func (e *Created) Handle(client *elasticsearch.Client) error {
	body := strings.NewReader(fmt.Sprintf(`{"name": "%s" }`, e.ProductName))
	res, err := client.Create("product", e.ProductID.String(), body)
	if err != nil {
		return err
	}
	err = res.Body.Close()
	return err
}

func (e *Created) GetName() string {
	return "ProductCreated"
}

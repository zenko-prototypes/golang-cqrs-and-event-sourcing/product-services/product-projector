package interfaces

import (
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/google/uuid"
)

type Event interface {
	Handle(client *elasticsearch.Client) error
	GetName() string
}

type BaseEvent struct {
	AggregateID uuid.UUID `json:"aggregate_id"`
}

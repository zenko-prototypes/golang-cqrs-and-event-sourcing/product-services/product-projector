# CQRS Projector

## Description

This microservice is here to handle a cache for the query microservice
It needs to be started before the two other.

## Installation

You need to update information about elastic search connection in this [file](elasticsearch/elasticsearch.go)
You need to update information about event store db connection in this [file](event_store/event_store.go)

## Run

```bash
$ go run main.go
```
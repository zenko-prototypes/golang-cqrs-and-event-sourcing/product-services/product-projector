package main

import (
	"ProductProjectorService/elasticsearch"
	"ProductProjectorService/event_store"
	"ProductProjectorService/product"
	"encoding/json"
	"fmt"
	"github.com/EventStore/EventStore-Client-Go/esdb"
	"log"
)

func main() {
	client, err := event_store.Connect()
	if err != nil {
		panic(err)
	}

	sub, err := event_store.Subscribe(client)
	if err != nil {
		panic(err)
	}

	defer func(sub *esdb.Subscription) {
		err := sub.Close()
		if err != nil {

		}
	}(sub)

	es, err := elasticsearch.Connect()
	if err != nil {
		log.Fatalf("Error creating the client: %s", err)
	}

	for {
		event := sub.Recv()

		if event.EventAppeared != nil {

			switch event.EventAppeared.Event.EventType {
			case "ProductCreated":
				e := &product.Created{}
				err := json.Unmarshal(event.EventAppeared.Event.Data, e)
				if err != nil {
					log.Fatalf("Error when parsing the event: %s", err)
				}
				err = e.Handle(es)
				if err != nil {
					log.Fatalf("Error when handling the event: %s", err)
				}
				break
			case "ProductNameUpdated":
				e := &product.NameUpdated{}
				err := json.Unmarshal(event.EventAppeared.Event.Data, e)
				if err != nil {
					log.Fatalf("Error when parsing the event: %s", err)
				}
				err = e.Handle(es)
				if err != nil {
					log.Fatalf("Error when handling the event: %s", err)
				}
				break
			default:
				fmt.Printf("Event not handled")
				break

			}
		}

		if event.SubscriptionDropped != nil {
			break
		}
	}
}
